import Papa from 'papaparse';

const parseStopsTxtWithPapa = (data) => {
    const firstLine = data.split('\n', 1)[0];
    const newHeader = firstLine.replace(/stop_/g, '');
    data = data.replace(firstLine, newHeader);
    return Papa.parse(data, {
        header: true,
    }).data;
}

export default parseStopsTxtWithPapa;
