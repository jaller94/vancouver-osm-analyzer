export function includesBusStopNumber(str) {
    return /[56]\d{4}/.test(str);
}

export function isBusStopNumber(str) {
    return /^[56]\d{4}$/.test(str);
}

export function parseBusStopNumber(str) {
    return Number.parseInt(str.match(/[56]\d{4}/)[0]);
}
