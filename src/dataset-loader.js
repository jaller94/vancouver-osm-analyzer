import fs from 'node:fs/promises';

const loadDataSet = async(name) => {
    const content = await fs.readFile(`../data/${name}.json`, 'utf8');
    return JSON.parse(content);
};

export default loadDataSet;
