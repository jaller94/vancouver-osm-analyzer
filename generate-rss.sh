#!/bin/bash

cd data
mv bus.json bus.old.json
mv fire_hydrant.json fire_hydrant.old.json
cd ../src
node rss/new-fire-hydrants.js
